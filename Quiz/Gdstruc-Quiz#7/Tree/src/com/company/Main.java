package com.company;

public class Main {

    public static void main(String[] args) {

        Trees tree = new Trees();

        tree.insert(-10);
        tree.insert(154);
        tree.insert(43);
        tree.insert(27);
        tree.insert(55);
        tree.insert(122);
        tree.insert(31);
        tree.insert(-22);
        tree.insert(2);

        tree.traverseInDescendingOrder();

        System.out.println("Minimum value is: " + tree.getMin());
        System.out.println("Maximum value is: " + tree.getMax());

    }
}