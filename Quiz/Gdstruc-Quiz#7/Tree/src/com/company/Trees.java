package com.company;

public class Trees {

    private Node root;

    public Node insert(int value)
    {
        if(root == null)
        {
            root = new Node(value);
        }
        else
        {
            root.insert(value);
        }
        return null;
    }

    public void traverseInDescendingOrder()
    {
        if(root != null)
        {
            root.traverseInOrderDescending();
        }
    }

    public Node getMin()
    {
        if(root != null)
        {
            return root.getMin();
        }
        return null;
    }

    public Node getMax()
    {
        if(root != null)
        {
            return root.getMax();
        }
        return null;
    }
}