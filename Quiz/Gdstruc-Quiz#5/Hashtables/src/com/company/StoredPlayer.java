package com.company;

public class StoredPlayer {
    public String key;
    public Players value;

    public StoredPlayer(String key, Players value) {
        this.key = key;
        this.value = value;
    }
}
