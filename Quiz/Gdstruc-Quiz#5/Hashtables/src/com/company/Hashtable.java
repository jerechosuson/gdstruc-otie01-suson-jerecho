package com.company;

public class Hashtable {
    private StoredPlayer[] hashtable;

    public Hashtable()
    {
        hashtable = new StoredPlayer[10];
    }

    private int hashKey(String key)
    {
        return key.length() % hashtable.length;
    }

    public void put(String key, Players value)
    {
        int hashedKey = hashKey(key);

        if(isOccupied(hashedKey))
        {
            int stoppingIndex = hashedKey;

            if(hashedKey == hashtable.length - 1)
            {
                hashedKey = 0;
            }
            else
            {
                hashedKey++;
            }

            while (isOccupied(hashedKey) && hashedKey != stoppingIndex)
            {
                hashedKey = (hashedKey + 1) % hashtable.length;
            }
        }

        if (isOccupied(hashedKey))
        {
            System.out.println("The position is already taken by another element" + hashedKey);
        }
        else
        {
            hashtable[hashedKey] = new StoredPlayer(key, value);
        }
    }

    public Players get(String key)
    {
        int hashedKey = findKey(key);

        if (hashedKey == -1)
        {
            return null;
        }
        return hashtable[hashedKey].value;
    }

    public Players remove(String key)
    {
        int hashedKey = findKey(key);

        if (hashedKey == -1)
        {
            return null;
        }

        Players removePlayer = hashtable[hashedKey].value;
        hashtable[hashedKey].value = null;hashtable[hashedKey] = null;
        return removePlayer;
    }

    private int findKey(String key)
    {
        int hashedKey = hashKey(key);

        if(hashtable[hashedKey] != null &&
                hashtable[hashedKey].key.equals(key))
        {
            return hashedKey;
        }


        int stoppingIndex = hashedKey;

        if (hashedKey == hashtable.length - 1) {
            hashedKey = 0;
        } else {
            hashedKey++;
        }

        while (hashtable[hashedKey] != null
                && !hashtable[hashedKey].key.equals(key)
                && hashedKey != stoppingIndex)
        {
            hashedKey = (hashedKey + 1) % hashtable.length;
        }

        if (hashtable[hashedKey] != null && hashtable[hashedKey].key.equals(key))
        {
            return hashedKey;
        }

        return -1;

    }

    public void printHashtable()
    {
        for (int i = 0; i < hashtable.length; i++)
        {
            if (hashtable[i] != null) {
                System.out.println("Element " + i + " " + hashtable[i].value);
            }
            else
            {
                System.out.println("Element " + i + " null");
            }
        }
    }

    private boolean isOccupied(int index)
    {
        return hashtable[index] != null;
    }
}
