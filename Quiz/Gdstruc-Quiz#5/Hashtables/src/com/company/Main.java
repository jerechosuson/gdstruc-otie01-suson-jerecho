package com.company;

public class Main {

    public static void main(String[] args) {
        Players resubi = new Players(1, "Resubi", 10);
        Players timmy = new Players(2, "Timmy", 25);
        Players governor = new Players(3, "Governor", 30);
        Players wardell = new Players(4, "Wardell", 36);
        Players sinatra = new Players(5, "Sinatra", 38);


        Hashtable hashtable = new Hashtable();
        hashtable.put(resubi.getName(), resubi);
        hashtable.put(timmy.getName(), timmy);
        hashtable.put(governor.getName(), governor);
        hashtable.put(wardell.getName(), wardell);
        hashtable.put(sinatra.getName(), sinatra);

        hashtable.printHashtable();

        System.out.println(hashtable.get("Governor"));
        System.out.println(hashtable.remove("Governor"));

        hashtable.printHashtable();
    }
}
