package com.company;
import java.util.ArrayList;
class Radiant
{
    int Radiantno;
    String name, Mainchar;

    public Radiant(int Radiantno, String name,
                   String Mainchar)
    {
        this.Radiantno = Radiantno;
        this.name = name;
        this.Mainchar = Mainchar;
    }

    public String toString()
    {
        return this.Radiantno + " " + this.name +
                " " + this.Mainchar;
    }
}


public class Main {
    public static void main(String[] args) {
        ArrayList<String> arr = new ArrayList<String>(6);


        Player resubi = new Player(1, "Resubi", 10);
        Player timmy = new Player(2, "Timmy", 25);
        Player governor = new Player(3, "Governor", 30);
        Player wardell = new Player(4, "Wardell", 36);
        Player sinatra = new Player(5, "Sinatra", 38);
        Player subroza = new Player(6, "Subroza", 34);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        playerLinkedList.addToFront(resubi);
        playerLinkedList.addToFront(timmy);
        playerLinkedList.addToFront(governor);
        playerLinkedList.addToFront(wardell);
        playerLinkedList.addToFront(sinatra);
        playerLinkedList.addToFront(subroza);

        playerLinkedList.printList();
        System.out.println("");

        arr.add("Resubi,");
        arr.add("Timmy,");
        arr.add("Governor,");
        arr.add("Wardell,");
        arr.add("Sinatra,");
        arr.add("Subroza");
        System.out.print("The Top 500 Radiants in Valorant are: ");
        for (String value : arr) {
            System.out.print(value);
            System.out.print(" ");
        }

        int pos = arr.indexOf(5);

        System.out.println("\nThe element 4 is at index : " + pos);
        ArrayList<Radiant> ar = new ArrayList<Radiant>();
        ar.add(new Radiant(309, "Resubi", "Yoru"));
        ar.add(new Radiant(476, "Timmy", "Sage"));
        ar.add(new Radiant(411, "Governor", "Reyna"));
        ar.add(new Radiant(79, "Wardell", "Jett"));
        ar.add(new Radiant(73, "Sinatra", "Sova"));
        ar.add(new Radiant(68, "Subroza", "Pheonix"));

        System.out.println("");

        System.out.println("TOP // NAME // MAIN");
        for (int i=0; i<ar.size(); i++)
            System.out.println(ar.get(i));


    }
}





