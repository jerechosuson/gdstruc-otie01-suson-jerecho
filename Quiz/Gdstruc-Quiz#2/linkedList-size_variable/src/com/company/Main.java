package com.company;
import java.util.LinkedList;


public class Main {

    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<String>();
        list.add("Resubi");
        list.add("Timmy");
        list.add("Governor");


        Player Resubi = new Player(1, "Resubi",10);
        Player Timmy = new Player(2, "Timmy",25);
        Player Governor = new Player(3, "Governor",30);



        PlayerLinkedList PlayerlinkedList = new PlayerLinkedList();

        PlayerlinkedList.addToFront(Resubi);
        PlayerlinkedList.addToFront(Timmy);
        PlayerlinkedList.addToFront(Governor);
        System.out.println("LinkedList: " + list);
        System.out.println("The size of the list is: " + list.size());

        PlayerlinkedList.printList();


    }
}
