package com.company;

public class PlayerNode {
    private Player player;
    private PlayerNode nextPlayer;

    public PlayerNode(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public PlayerNode getNextplayer() {
        return nextPlayer;
    }

    public void setNextplayer(PlayerNode nextplayer) {
        this.nextPlayer = nextplayer;
    }
}
