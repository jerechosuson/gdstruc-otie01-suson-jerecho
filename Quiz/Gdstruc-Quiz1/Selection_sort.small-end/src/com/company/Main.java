package com.company;

public class Main {

    public static void main(String[] args) {
        int[] numbers = new int[10];

        numbers[0] = 2;
        numbers[1] = 23;
        numbers[2] = 53;
        numbers[3] = 44;
        numbers[4] = 12;
        numbers[5] = 24;
        numbers[6] = -13;
        numbers[7] = 18;
        numbers[8] = 29;
        numbers[9] = 78;

        System.out.println("Before selection sort:");
        printArrayElements(numbers);
        selectionSort(numbers);
        System.out.println("\nAfter selection sort:");
        printArrayElements(numbers);
    }

    private static void selectionSort(int[] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            int largestIndex = 0;
            for (int i = 0; i < lastSortedIndex; i++) {

                if (arr[i] < arr[largestIndex])
                {
                    largestIndex = i;
                }
            }
            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[largestIndex];
            arr[largestIndex] = temp;
        }
    }
    private static void printArrayElements(int[] arr)
    {
        for (int j : arr)
            System.out.println(j + " ");
    }
}
