package com.company;

public class Main {

    public static void main(String[] args) {
        int[] numbers = new int[10];

        numbers[0] = 2;
        numbers[1] = 23;
        numbers[2] = 44;
        numbers[3] = 53;
        numbers[4] = 12;
        numbers[5] = 24;
        numbers[6] = -13;
        numbers[7] = 18;
        numbers[8] = 29;
        numbers[9] = 78;

        System.out.println("Before bubble sort:");
        printArrayElements(numbers);
        bubbleSort(numbers);
        System.out.println("\nAfter bubble sort:");
        printArrayElements(numbers);
    }

    private static void bubbleSort(int[] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            for (int i = 0; i < lastSortedIndex; i++)

                if (arr[i] < arr[i + 1])
                {
                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                }
            }
        }


    private static void printArrayElements(int[] arr)
    {
        for (int j : arr)
            System.out.println(j + " ");
        }
}
