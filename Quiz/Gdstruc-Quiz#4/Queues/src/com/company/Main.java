package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        ArrayQ matchQ = new ArrayQ(50);
        System.out.println("Players entering a match");

        for(int i = matchQ.size(); i < 50; i++)
        {
            matchQ.add(new Players(i+1, "Player(" + i + ")" , 1));
        }

        System.out.println();
        matchQ.printQueue();
        System.out.println();

        int lobbyFull = 0;
        Random rand = new Random();
        int upperbound = 7;

        System.out.println("Press Enter to Start Queue");
        new java.util.Scanner(System.in).nextLine();

        while (lobbyFull != 10)
        {
            int playersIn = (rand.nextInt(upperbound)) + 1;
            System.out.println("Full lobby #: " + lobbyFull);
            System.out.println("Players in match: " + playersIn);

            if (playersIn > 4)
            {
                System.out.println("Matchmaking: 5 players entered!");
                for (int i = 0; i < 5; i++)
                {
                    System.out.println("In Game: " + matchQ.remove());
                }
                lobbyFull++;
                System.out.println("Press enter to next");
                new java.util.Scanner(System.in).nextLine();
            }
            else
            {
                System.out.println("Players in the lobby is not enough... (Press enter to next)");
                new java.util.Scanner(System.in).nextLine();
            }
        }

        System.out.println("10 games has been reached");
    }
}