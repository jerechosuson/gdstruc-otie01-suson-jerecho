package com.company;

import java.util.NoSuchElementException;

public class ArrayQ
{
    private Players[] queue;
    private int front;
    private int back;

    public ArrayQ(int capacity)
    {
        queue = new Players[capacity];
    }

    public void add(Players player)
    {
        if (back == queue.length)
        {
            Players[] newArray = new Players[queue.length * 2];
            System.arraycopy(queue, 0, newArray, 0, queue.length);
            queue = newArray;
        }

        queue[back] = player;
        back++;
    }

    public Players remove()
    {
        if (size() == 0)
        {
            throw new NoSuchElementException();
        }

        Players removePlayer = queue[front];
        queue[front] = null;
        front++;

        if (size() == 0)
        {
            front = 0;
            back = 0;
        }
        return removePlayer;
    }

    public int size()
    {
        return back - front;
    }

    public Players peek()
    {
        if (size() == 0)
        {
            throw new NoSuchElementException();
        }

        return queue[front];
    }

    public void printQueue()
    {
        for (int i = front; i < back; i++)
        {
            System.out.println(queue[i]);
        }
    }
}