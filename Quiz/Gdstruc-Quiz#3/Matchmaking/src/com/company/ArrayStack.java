package com.company;

import java.util.NoSuchElementException;

public class ArrayStack {
    private Players[] stack;
    private int front;
    private int back;

    public ArrayStack(int capacity)
    {
        stack = new Players[capacity];
    }

    public void push(Players player)
    {
        if (back == stack.length)
        {
            Players[] newArray = new Players[stack.length * 2];
            System.arraycopy(stack, 0, newArray, 0, stack.length);
            stack = newArray;
        }

        stack[back] = player;
        back++;
    }

    public Players remove()
    {
        if (size() == 0)
        {
            throw new NoSuchElementException();
        }

        Players removePlayer = stack[front];
        stack[front] = null;
        front++;

        if (size() == 0)
        {
            front = 0;
            back = 0;
        }
        return removePlayer;
    }

    public int size()
    {
        return front - back;
    }

    public Players peek()
    {
        if (size() == 0)
        {
            throw new NoSuchElementException();
        }

        return stack[back];
    }

    public void printQueue()
    {
        for (int i = front; i < back; i++)
        {
            System.out.println(stack[i]);
        }
    }
}

