package com.company;
import java.util.*;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        JunkSet setC = new JunkSet();
        LinkedSet setA = new LinkedSet();
        PlayerSet setB = new PlayerSet();

        setA.push(new Card(1, "Pink"));
        setA.push(new Card(2, "Pink"));
        setA.push(new Card(3, "Pink"));
        setA.push(new Card(4, "Pink"));
        setA.push(new Card(5, "Pink"));
        setA.push(new Card(6, "Pink"));
        setA.push(new Card(7, "Pink"));
        setA.push(new Card(8, "Pink"));
        setA.push(new Card(9, "Pink"));
        setA.push(new Card(10, "Pink"));
        setA.push(new Card(11, "Pink"));
        setA.push(new Card(12, "Pink"));
        setA.push(new Card(13, "Pink"));
        setA.push(new Card(14, "Pink"));
        setA.push(new Card(15, "Pink"));
        setA.push(new Card(16, "Pink"));
        setA.push(new Card(1, "Purple"));
        setA.push(new Card(2, "Purple"));
        setA.push(new Card(3, "Purple"));
        setA.push(new Card(4, "Purple"));
        setA.push(new Card(5, "Purple"));
        setA.push(new Card(6, "Purple"));
        setA.push(new Card(7, "Purple"));
        setA.push(new Card(8, "Purple"));
        setA.push(new Card(9, "Purple"));
        setA.push(new Card(10, "Purple"));
        setA.push(new Card(11, "Purple"));
        setA.push(new Card(12, "Purple"));
        setA.push(new Card(13, "Purple"));
        setA.push(new Card(14, "Purple"));
        setA.push(new Card(15, "Purple"));
        setA.push(new Card(16, "Purple"));


        int sizeOfPcards;
        sizeOfPcards = 0;
        int sizeOfDeck;
        sizeOfDeck = 32;
        int sizeOfJunk = 0;

        while (sizeOfDeck != 0)
        {
            //Rng + Input
            Random command = new Random();
            int number = 0;
            System.out.println("Press enter to set and shuffle: ");
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();



            number = 1 + command.nextInt(3);
            //Player pull first a card
            if (sizeOfPcards == 0)
            {
                number = number = 1;
            }

            //RNG
            Random card = new Random();
            int totalCards;
            totalCards = 1 + card.nextInt(5);

            //Player pulls out cards from the deck
            if (number == 1)
            {
                System.out.println("Player pull out a card ");
                if (sizeOfDeck < totalCards)
                {
                    totalCards = totalCards = sizeOfDeck;
                }
                System.out.println("Player pull out a card " + totalCards + " cards from the deck!");
                sizeOfDeck = sizeOfDeck - totalCards;
                sizeOfPcards = sizeOfPcards + totalCards;
                for (int i = totalCards - 1; i >= 0; i--) {
                    setB.push(setA.pop());
                }
            }

            if (sizeOfPcards == 0)
            {
                System.out.println("Set of cards is not enough. Shuffle again!");
                totalCards = totalCards = 0;
            }
            //Player pulling from junk case
            if (number == 3) {
                System.out.println("Player pulls from the junk case!");
                if (sizeOfJunk == 0)
                {
                    System.out.println("Not enough junk cards. Shuffle again!");
                }
                if (sizeOfJunk < totalCards)
                {
                    totalCards = totalCards = sizeOfJunk;
                }
                if (sizeOfJunk >= totalCards) {
                    System.out.println("Player pull out a card " + totalCards + " cards from the junk deck!");
                    sizeOfJunk = sizeOfJunk - totalCards;
                    for (int i = totalCards - 1; i >= 0; i--) {
                        setB.push(setC.pop());
                    }
                }
            }


            //Player junk a card
            if (number == 2) {
                System.out.println("Player junk!");
                if (sizeOfPcards < totalCards)
                {
                    totalCards = totalCards = sizeOfPcards;
                }

                if (sizeOfPcards >= totalCards) {
                    System.out.println("Player junk " + totalCards + " cards on hold!");
                    sizeOfPcards = sizeOfPcards - totalCards;
                    sizeOfJunk = sizeOfJunk + totalCards;
                    for (int i = totalCards - 1; i >= 0; i--) {
                        setC.push(setB.pop());
                    }
                }
            }

            //Cards on hold, Sets on deck and Junks
            System.out.println("Present Cards in the Deck: " + sizeOfDeck);
            System.out.println("Set of Cards in the junk: " + sizeOfJunk);
            System.out.println("Set of Cards on Player: " + sizeOfPcards + "\n" + "Player's cards on hold: ");
            setB.printStack();

        }
        //If no cards left -> game over
        System.out.println("Game Over! Zero (0) cards left in the deck!");
    }
}

