package com.company;
import java.util.*;
import java.util.LinkedList;
import java.util.ListIterator;

public class JunkSet {

    private LinkedList<Card> setC;

    public JunkSet()
    {
        setC = new LinkedList<Card>();
    }
    public void push(Card card)
    {
        setC.push(card);
    }
    public void printStack()
    {
        ListIterator<Card> iterator = setC.listIterator();
        System.out.println("Printing sets");
        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }
    public boolean isEmpty()
    {
        return  setC.isEmpty();
    }

    public Card pop()
    {
        return setC.pop();
    }

    public Card peek()
    {
        return setC.peek();
    }
}
