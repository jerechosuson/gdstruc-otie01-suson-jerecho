package com.company;
import java.util.*;
import java.util.LinkedList;
import java.util.ListIterator;

public class PlayerSet {

    private LinkedList<Card> setB;

    public PlayerSet()
    {
        setB = new LinkedList<Card>();
    }
    public boolean isEmpty()
    {
        return  setB.isEmpty();
    }
    public Card pop()
    {
        return setB.pop();
    }
    public Card peek()
    {
        return setB.peek();
    }
    public void push(Card card)
    {
        setB.push(card);
    }


    public void printStack()
    {
        ListIterator<Card> iterator = setB.listIterator();
        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }
}
