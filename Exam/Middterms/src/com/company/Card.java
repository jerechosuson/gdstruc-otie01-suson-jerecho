package com.company;
import java.util.Objects;

public class Card {
    private int cardNumber;
    private String cardColor;

    public Card(int cardNumber, String cardColor) {
        this.cardNumber = cardNumber;
        this.cardColor = cardColor;
    }

    @Override
    public String toString() {
        return "Card{" +
                "cardNumber=" + cardNumber +
                ", cardColor='" + cardColor + '\'' +
                '}';
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardColor() {
        return cardColor;
    }

    public void setCardColor(String cardColor) {
        this.cardColor = cardColor;
    }
}
