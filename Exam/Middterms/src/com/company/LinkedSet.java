package com.company;
import java.util.*;
import java.util.LinkedList;
import java.util.ListIterator;

public class LinkedSet {

    private LinkedList<Card> setA;
    private int data;
    private LinkedSet next;

    public LinkedSet()
    {
        setA = new LinkedList<Card>();
    }
    public void push(Card card)
    {
        setA.push(card);
    }

    public boolean isEmpty()
    {
        return  setA.isEmpty();
    }

    public Card pop()
    {
        return setA.pop();
    }

    public Card peek()
    {
        return setA.peek();
    }

    public void userInput()
    {
        System.out.println("Input a text: ");
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();
        System.out.println("You have entered; " + input);
    }
    public void printStack()
    {
        ListIterator<Card> iterator = setA.listIterator();

        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }
    public class Node {

        int data;
        Node next;
    }

    public Node head;
    public Node tail;
    public int size;

    public void display() {

        Node temp = this.head;
        while (temp != null) {
            System.out.println(temp.data + " ");
            temp = temp.next;
        }
    }
    public int getFirst() throws Exception {

        if (this.size == 0) {

            throw new Exception("linked list has no data");
        }

        return this.head.data;
    }

    public int getLast() throws Exception {

        if (this.size == 0) {

            throw new Exception("linked list has no data");
        }
        return this.tail.data;
    }

    public void addFirst(int item) {

        Node nn = new Node();

        nn.data = item;
        if (this.size == 0) {
            this.head = nn;
            this.tail = nn;
            this.size = this.size + 1;

        } else {

            nn.next = this.head;

            this.head = nn;

            this.size = this.size + 1;

        }
    }
}
